import json
import boto3

s3 = boto3.client('s3')


def lambda_handler(event, context):
    print("Incoming Event: ", event)
    bucket = event['Records'][0]['s3']['bucket']['name']
    print("Bucket Name: ", bucket)
    # key =  urllib.parse.unquote_plus(event['Records'][0]['s3']['bucket']['name'],encoding='utf-8')
    key = event['Records'][0]['s3']['object']['key']
    print("Filename: ", key)
    input_data = read_data(bucket, key)
    print("Input Data: ", input_data)
    output_data = parse_data(input_data)
    dump_data_to_s3(bucket, 'outputFile.csv', output_data)

    parsed_data = read_data(bucket, 'outputFile.csv')


def read_data(bucket, key):
    try:
        response = s3.get_object(Bucket=bucket, Key=key)

        text = response["Body"].read().decode()
        data = json.loads(text)

        print(data)
        return data

    except Exception as e:
        print(e)
        return "Data not available"


def parse_data(data):
    parsed_data = {}
    print("Data[Records]: ", data['Records'])
    for i in range(len(data['Records'])):
        try:
            uname = data['Records'][i]['userIdentity']['sessionContext']['sessionIssuer']['userName']
        except:
            uname = "NA in logs"
        try:
            eventname = data['Records'][i]['eventName']
        except:
            eventname = "NA in logs"
        try:
            eventtime = data['Records'][i]['eventTime']
        except:
            eventtime = "NA in logs"
        try:
            eventsource = data['Records'][i]['eventSource']
        except:
            eventsource = "NA in logs"
        try:
            eventtype = data['Records'][i]['eventType']
        except:
            eventtype = "NA in logs"

        parsed_data[i] = uname + ',' + eventname + ',' + eventtime + ',' + eventsource + ',' + eventtype

    try:
        output_json = json.dumps(parsed_data).encode('utf-8')
        return output_json
    except Exception as e:
        print("Excption Encountered: ", e)
        return "Not available"


def dump_data_to_s3(bucket, key, data):
    s3.put_object(Bucket=bucket, Key=key, Body=data)
    print("Data_dump complete")